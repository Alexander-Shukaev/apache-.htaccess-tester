<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Apache '.htaccess' Tester</title>
  <link rel="stylesheet" src="//normalize-css.googlecode.com/svn/trunk/normalize.css" />
  <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body id="home">
<?php
echo "<h1>Apache <tt>.htaccess</tt> Tester</h1>";
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) === false) {
  echo "<p><tt>mod_rewrite</tt> is on!</p>";
  echo "<p>The <tt>" . $_SERVER['REQUEST_URI'] . "</tt> path was used to request this page.</p>";
} elseif (is_readable(__DIR__ . '/.htaccess')) {
  echo "<p>The <tt>.htaccess</tt> file exists and is readable to the web server.</p>";
  echo "<p><strong>Contents:</strong> </p>";
  echo "<p><textarea style='width: 640px; height: 480px;'>";
  echo file_get_contents(__DIR__ . '/.htaccess');
  echo "</textarea></p>";
} else {
  echo "<p><strong>Error:</strong> The <tt>.htaccess</tt> file does not exist or is not readable to the web server.</p>";
  die();
}
?>
</body>
</html>